package main

import (
	"os"
	"time"

	"github.com/go-redis/redis"
)

var client *redis.Client

// Give us some seed data
func init() {
	addr := os.Getenv("REDIS")

	if addr == "" {
		addr = ":6379"
	}

	client = redis.NewClient(&redis.Options{
		Addr:         addr,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
	})
}

func RepoIncrBy(key string, value int64) error {

	_, err := client.IncrBy(key, value).Result()

	return err
}
