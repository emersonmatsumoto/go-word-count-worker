package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strings"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

func Process(fileName string) {
	m := make(map[string]int64)

	file, err := os.Open("/upload/" + fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	decodingReader := transform.NewReader(file, charmap.Windows1252.NewDecoder())
	scanner := bufio.NewScanner(decodingReader)

	for scanner.Scan() {
		line := scanner.Text()
		words := WordSplit(line)

		for _, element := range words {

			if element == "" {
				continue
			}

			if val, ok := m[element]; ok {
				m[element] = val + 1
			} else {
				m[element] = 1
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	for k, v := range m {
		RepoIncrBy(k, v)
	}

	log.Printf("Done")
}

func WordSplit(line string) []string {
	reg := regexp.MustCompile("[\"'<>{}()\\[\\]\\/`´*!?.,:;=]+")
	replaceStr := reg.ReplaceAllString(line, " ")
	replaceStr = strings.TrimSpace(replaceStr)
	replaceStr = strings.ToLower(replaceStr)
	words := strings.Split(replaceStr, " ")

	return words
}
