package main

type Word struct {
	Key   string `json:"key"`
	Count int    `json:"count"`
}
